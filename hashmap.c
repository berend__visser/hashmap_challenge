#include "hashmap.h"

int hash(HashMap * hm, char * key) {
	int sum = 0;
	int len = strlen(key) + 1;

	for (int i = 0; i < len; i++) {
		sum = sum + key[i];
	}

	return sum % hm->size;
}

HashMap * create_hashmap(size_t key_size) {
	assert(key_size > 0);

	HashMap * hm = (HashMap *) malloc(sizeof(HashMap));
	assert(hm);

	hm->buckets = calloc(key_size, sizeof(Bucket));
	assert(hm->buckets);

	hm->size  = key_size; 

	return hm;
}

void insert_data(HashMap * hm, char * key, void * data, ResolveCollisionCallback resolve_collision) {
	
	//calculate hash of key
	int bucket_index = hash(hm,key);

	//allocate memory for the key and data
	char * key_dest = malloc(strlen(key)+1);
	strcpy(key_dest, key);	
	
	//check if initial bucket has been allocated
	if(hm->buckets[bucket_index] == NULL){
		hm->buckets[bucket_index] = malloc(sizeof(Bucket));
		
		hm->buckets[bucket_index]->next = NULL;
		hm->buckets[bucket_index]->key = key_dest;
		hm->buckets[bucket_index]->data = data;
		return;
	}//loop over existing buckets
	else{
		
		Bucket * current_bucket = hm->buckets[bucket_index];
		//check if new key is the same as an old key
		if(strcmp(current_bucket->key, key) == 0){
		
			//let the application decide how to resolve the collision
			current_bucket->data  = resolve_collision(current_bucket->data, data);
			return;
		}
		//check if more buckets exist
		while (current_bucket->next != NULL)
		{
			//move to next bucket
			current_bucket = current_bucket->next;
			if(strcmp(current_bucket->key, key) == 0){
				//let the application decide how to resolve the collision
				current_bucket->data  = resolve_collision(current_bucket->data, data);
				return;
			}
			
		} 

		//create and append new bucket
		Bucket * new_bucket = malloc(sizeof(Bucket));

		new_bucket->next = NULL;
		new_bucket->key = key_dest;
		new_bucket->data = data;
		
		current_bucket->next = new_bucket;
		
	}
}
	

void * get_data(HashMap * hm, char * key) {
	int bucket_index = hash(hm, key);
	

	Bucket * current_bucket = hm->buckets[bucket_index];
	
	//loop over buckets
	while(current_bucket != NULL){
		//check if key matches
		if(strcmp(current_bucket->key, key)==0){
			return current_bucket->data;
		}
		current_bucket = current_bucket->next;
	}
	//if no keys matched return NULL
	return NULL;
}

void iterate(HashMap * hm, void (* callback)(char * key, void * data)) {
	//loop over all buckets
	for(size_t i = 0; i<hm->size; i++){
		Bucket * current_bucket = hm->buckets[i];
		//loop of linked list
		while(current_bucket != NULL){
			callback(current_bucket->key,current_bucket->data);
			current_bucket = current_bucket->next;
		}
	}
}

void remove_data(HashMap * hm, char * key, DestroyDataCallback destroy_data) {
	int bucket_index = hash(hm, key);
	//check if first bucket is null
	if(hm->buckets[bucket_index] == NULL){
		return;
	}
	
	if(strcmp(hm->buckets[bucket_index]->key, key)==0){
		//remove bucket
		//deallocate memory of the key
		free(hm->buckets[bucket_index]->key);

		//let the user of the hashmap decide what to do with the datapointer
		if(destroy_data != NULL){
			destroy_data(hm->buckets[bucket_index]->data);
		}
		//let subsequent item in linked list now be the first
		if(hm->buckets[bucket_index] != NULL){
			hm->buckets[bucket_index] = hm->buckets[bucket_index]->next;
		}
	}
	else{
		Bucket * current_bucket = hm->buckets[bucket_index];
		Bucket * previous_bucket = NULL;
		while(1){
			//check if next bucket exists
			if(current_bucket->next == NULL){
				return;
			}
			else{
				previous_bucket = current_bucket;
				current_bucket = current_bucket->next;
				if(strcmp(current_bucket->key, key)==0){
					//deallocate memory of the key
					free(current_bucket->key);
					//check if data function exists
					if(destroy_data != NULL){
						destroy_data(current_bucket->data);
					}
					
					if(current_bucket->next != NULL){
						previous_bucket->next = current_bucket->next;
					}
					else{
						previous_bucket->next = NULL;
					}
					return;
				}
			}
		}
	}
}

void delete_hashmap(HashMap * hm, DestroyDataCallback destroy_data) {
	
	//loop over all buckets
	for(size_t i = 0; i<hm->size; i++){
		Bucket * current_bucket = hm->buckets[i];
		//loop over linked list within a bucket
		while(current_bucket != NULL){
			remove_data(hm,current_bucket->key, destroy_data);
			current_bucket = current_bucket->next;

		}
	}
}
